//GET BITACORA FROM DB
@=G_BITACORA = '';
$query=executeQuery(
	"SELECT
		STAGE,
        FILENAME,
        FILE_EVENT,
        DATE_EVENT
	FROM
		PMT_PRP_REQUEST_FILES_LOG
	WHERE
		CASE_NUMBER = '".@@APP_NUMBER."'
	AND
		STAGE = '" . @@VALIDATION_STAGE . "'"
);
for($i = 1; $i <=  count($query); $i++){
    @=G_BITACORA[$i] = array(
		'S_BITACORA_STAGE'=> $query[$i]['STAGE'],
		'S_BITACORA_FILE'=>$query[$i]['FILENAME'],
		'S_BITACORA_EVENT'=>$query[$i]['FILE_EVENT'],
		'S_BITACORA_DATE'=>$query[$i]['DATE_EVENT']);
}
$CASE_NUMBER = @@APP_NUMBER;
$REQUEST=executeQuery(
    "SELECT
        COMMENTS
    FROM
        PMT_PRP_COMMENTS_PERFORMANCE
    WHERE
        CASE_NUMBER = '" . $CASE_NUMBER . "'
    ORDER BY
        ID DESC
    LIMIT 1
");
$aUser = PMFInformationUser(@@USER_LOGGED);
@@USR_NAME = $aUser['firstname'] .' '. $aUser['lastname'];
$COMMENTS = $REQUEST[1]['COMMENTS'] . ' | ' . date("Y-m-d H:i:s") . ', ' . @@USR_NAME . ', ' . @@S_COMMENTS;
if(@@S_COMMENTS <> "") {
    executeQuery("DELETE FROM PMT_PRP_COMMENTS_PERFORMANCE WHERE CASE_NUMBER = '" . $CASE_NUMBER . "'");
    $query = 
    "INSERT INTO PMT_PRP_COMMENTS_PERFORMANCE
    (
        ID, 
        CASE_NUMBER,
        COMMENTS,
        STATUS
    ) 
    VALUES 
    (
        NULL, 
        '$CASE_NUMBER',
        '$COMMENTS',
        1
    )";
    executeQuery($query);
}
$groupName = "04PJ ENGINEERING";
$groupEngineering = PMFGetGroupUID($groupName);

$groupName = "04PJ QSE DIRECTOR";
$groupQse = PMFGetGroupUID($groupName);

$groupName = "04PJ QUALITY";
$groupQuality = PMFGetGroupUID($groupName);

$groupName = "04PJ TOM";
$groupTom = PMFGetGroupUID($groupName);

//CASE SELECTION FOR USERS APPROVAL
switch (@@S_VALIDATION_TYPE) {
    case '3':
	case '2':
	case '6':
	case '8':        
		@@S_GROUP_DESIGN = $groupTom;
		@@S_GROUP_INSTALLATION = $groupTom;
		@@S_GROUP_OPERATION = $groupTom;
		@@S_GROUP_QSE = $groupQse;
		@@S_GROUP_PERFORMANCE = $groupQse;
        break;
    case '4':
	case '7':	
		@@S_GROUP_DESIGN = $groupEngineering;
		@@S_GROUP_INSTALLATION = $groupQse;
		@@S_GROUP_OPERATION = $groupQse;
		@@S_GROUP_QSE = $groupQse;
		@@S_GROUP_PERFORMANCE = $groupQse;
        break;	
	case '10':	
		@@S_GROUP_DESIGN = $groupQse;
		@@S_GROUP_INSTALLATION = $groupQse;
		@@S_GROUP_OPERATION = $groupQse;
		@@S_GROUP_QSE = $groupQse;
		@@S_GROUP_PERFORMANCE = $groupQse;
        break;		
    case '1':
	case '9':	
		@@S_GROUP_DESIGN = $groupEngineering;
		@@S_GROUP_INSTALLATION = $groupEngineering;
		@@S_GROUP_OPERATION = $groupQse;
		@@S_GROUP_QSE = $groupQse;
		@@S_GROUP_PERFORMANCE = $groupQse;
        break;	
	case '5':	
		@@S_GROUP_DESIGN = $groupTom;
		@@S_GROUP_INSTALLATION = $groupTom;
		@@S_GROUP_OPERATION = $groupTom;
		@@S_GROUP_QSE = $groupTom;
		@@S_GROUP_PERFORMANCE = $groupTom;
        break;		
    default:        
}
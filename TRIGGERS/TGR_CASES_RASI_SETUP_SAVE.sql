// AUTOR: BSTI 
// www.BSTI.com.mx

$caseNumber     = @@APP_NUMBER;
$typeId         = @@S_TYPE;
$validationType = @@S_VALIDATION_TYPE;

$REQUEST=executeQuery(
    "SELECT
        ENGENEERING_CHECK_USR,
        QUALITY_CHECK_USR
    FROM
        PMT_PJ_PROCESS_RASI_SETUP
    WHERE
        TYPE_ID = '" . $typeId . "'
    LIMIT 1
");
$tomUserId          = @@S_TOM_DESIGNATED;
$engineeringUserId  = $REQUEST[1][ENGENEERING_CHECK_USR];
$qseUserId          = $REQUEST[1][QUALITY_CHECK_USR];
/*
01 New Facility
02 Conversion in existing Line (CSD to Stills)
03 New CSD line
04 New Production Line Stills, Plant Based & Dairy
05 New Beverage
06 New Process
07 Equipment Change
08 New Technology		
09 Process Change Tipo 1
10 Process Change Tipo 2
*/
@=S_GROUP_DESIGN = '';
@=S_GROUP_INSTALLATION = '';
@=S_GROUP_OPERATION = '';
@=S_GROUP_PERFORMANCE = '';
@=S_GROUP_QSE = '';
switch ($validationType) {
    case 2:     //  02 Conversion in existing Line (CSD to Stills)
    case 3:     //  03 New CSD line
    case 7:     //  07 Equipment Change
        @=S_GROUP_DESIGN = $tomUserId;
        @=S_GROUP_INSTALLATION = $tomUserId;
        @=S_GROUP_OPERATION = $tomUserId;
        @=S_GROUP_PERFORMANCE = $qseUserId;
        @=S_GROUP_QSE = $qseUserId;
        break;
    case 4:     //  04 New Production Line Stills, Plant Based & Dairy
    case 6:     //  06 New Process
    case 8:     //  08 New Technology
        @=S_GROUP_DESIGN = $engineeringUserId;
        @=S_GROUP_INSTALLATION = $qseUserId;
        @=S_GROUP_OPERATION = $qseUserId;
        @=S_GROUP_PERFORMANCE = $qseUserId;
        break;			
    case 5:     //  05 New Beverage
        @=S_GROUP_DESIGN = $tomUserId;
        @=S_GROUP_INSTALLATION = $tomUserId;
        @=S_GROUP_OPERATION = $qseUserId;
        @=S_GROUP_PERFORMANCE = $qseUserId;
        break;
    case 1:     //  01 New Facility
        @=S_GROUP_DESIGN = $engineeringUserId;
        @=S_GROUP_INSTALLATION = $engineeringUserId;
        @=S_GROUP_OPERATION = $qseUserId;
        @=S_GROUP_PERFORMANCE = $qseUserId;
        break;			
    case 9:     //  09 Process Change Tipo 1
        @=S_GROUP_DESIGN = $qseUserId;
        @=S_GROUP_INSTALLATION = $qseUserId;
        @=S_GROUP_OPERATION = $qseUserId;
        @=S_GROUP_PERFORMANCE = $qseUserId;
        break;
    case 10:    //  10 Process Change Tipo 2
        @=S_GROUP_DESIGN = $tomUserId;
        @=S_GROUP_INSTALLATION = $tomUserId;
        @=S_GROUP_OPERATION = $tomUserId;
        @=S_GROUP_PERFORMANCE = $tomUserId;
        break;			
}
$S_GROUP_DESIGN = @@S_GROUP_DESIGN;
$S_GROUP_INSTALLATION = @@S_GROUP_INSTALLATION;
$S_GROUP_OPERATION = @@S_GROUP_OPERATION;
$S_GROUP_PERFORMANCE = @@S_GROUP_PERFORMANCE;
$S_GROUP_QSE = @@S_GROUP_QSE;

$query = 
"INSERT INTO PMT_PJ_CASES_RASI_SETUP
(
    ID, 
    CASE_NUMBER,
    DESIGN_CHECK_USR,
    INSTALLATION_CHECK_USR,
    OPERATION_CHECK_USR,
    PERFORMANCE_CHECK_USR,
    QSE_CHECK_USR,
    STATUS
) 
VALUES 
(
    NULL,
    '$caseNumber',
    '$S_GROUP_DESIGN',
    '$S_GROUP_INSTALLATION',
    '$S_GROUP_OPERATION',
    '$S_GROUP_PERFORMANCE',
    '$S_GROUP_QSE',
    '1'
)";		

executeQuery($query);
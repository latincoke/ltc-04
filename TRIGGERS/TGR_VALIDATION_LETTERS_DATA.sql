$S_PLANT_ID = @@S_PLANT_ID;
@=D_APPROVAL_DATE = date("d/m/Y");
$LETTERS_DATA=executeQuery(
    "(SELECT
        1 as PRIORITY,
        VLD.ID as ID,
        VLD.REQUESTOR_NAME,
        VLD.CASE_NUMBER,
        VLD.PLANT_MANAGER,
        VLD.PROCESS,
        VLD.TYPE,
        TYPE.ORDEN as ORDEN,
        VLD.STATUS,
        VLD.APPROVAL_DATE,
        VLD.LINE,
        VLD.PACKAGE_TYPE,
        NULL as S_PERFORMANCE_COMMENTS_KO,
        CONCAT(VLD.DESCRIPTION, VLD.COMMENTS) as DESCRIPTION,
        VLD.BRAND as BRAND,
        VLD.MODEL as MODEL,
        VLD.BBN as BBN,
        VLD.THERMAL_TREATMENT as THERMAL_TREATMENT,
        VLD.PRODUCT_NAME as PRODUCT_NAME
    FROM
        PMT_PRP_VALIDATION_LETTERS_DATA VLD
    LEFT JOIN
        PMT_PJ_PROJECT_TYPES TYPE ON VLD.TYPE = TYPE.INDEX_TYPE
    WHERE
        PLANT_ID = " . $S_PLANT_ID . " AND
        (VLD.STATUS = 'APPROVED' OR
         VLD.STATUS = 'CONDITIONED')
    ) UNION ALL (
    SELECT
        2 as PRIORITY,
        R.ID as ID,
        R.S_REQUESTER_NAME as REQUESTOR_NAME,
        R.CASE_NUMBER as CASE_NUMBER,
        R.S_PLANT_MANAGER_NAME as PLANT_MANAGER,
        RD.S_PROJECT_TYPE as PROCESS,
        TYPE.INDEX_TYPE as TYPE,
        TYPE.ORDEN as ORDEN,
        R.STATUS as STATUS,
        CONCAT(MONTHNAME(QSE.D_APPROVAL_DATE), ' ' , YEAR(QSE.D_APPROVAL_DATE)) as APPROVAL_DATE,
        RD.S_LINE as LINE,
        RD.S_PACKAGE_TYPE as PACKAGE_TYPE,
        RD.S_PERFORMANCE_COMMENTS_KO,
        RD.S_DESCRIPTION as DESCRIPTION,
        RD.S_BRAND as BRAND,
        RD.S_MODEL as MODEL,
        RD.S_BBN as BBN,
        RD.S_THERMAL_TREATMENT as THERMAL_TREATMENT,
        RD.S_PRODUCT_NAME as PRODUCT_NAME
    FROM
        PMT_PRP_REQUEST R
    LEFT JOIN
        PMT_PRP_REQUEST_DETAIL RD ON R.CASE_NUMBER = RD.CASE_NUMBER
    LEFT JOIN
        PMT_PJ_PROJECT_TYPES TYPE ON RD.S_TYPE = TYPE.ID
    LEFT JOIN
        PMT_PRP_REQUEST_QSE QSE ON R.CASE_NUMBER = QSE.CASE_NUMBER
    WHERE
        S_PLANT_ID = " . $S_PLANT_ID . " AND
        (R.STATUS = 'PERFORMANCE APPROVED' OR
         R.STATUS = 'APPROVED' OR
         R.STATUS = 'CONDITIONED' OR 
         R.STATUS = 'Performance conditioned')
    ) ORDER BY
        ORDEN, PRIORITY, ID, APPROVAL_DATE
");
$ASEBI_DETAIL=executeQuery(
    "SELECT
        CASE_NUMBER,
        PRODUCT_NAME,
        SPEED,
        VOLUME,
        PACKAGE_TYPE
    FROM
        PMT_PRP_REQUEST_ASEBI_DETAILS ASEBI
    WHERE
        PLANT_ID = " . $S_PLANT_ID . "
");
@=S_REQUESTER_NAME = $LETTERS_DATA[1]['REQUESTOR_NAME'];
@=S_PLANT_MANAGER_NAME = $LETTERS_DATA[1]['PLANT_MANAGER'];

$VALIDATION_LETTERS_DATA_DETAILS = '';
foreach ($LETTERS_DATA as $key => $LETTER_DATA) {
    if($key != 1 AND $LETTERS_DATA[$key]['PROCESS'] != $LETTERS_DATA[$key - 1]['PROCESS']) {
        $VALIDATION_LETTERS_DATA_DETAILS .= '</tbody></table>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<p></p><p></p>';
    }
    if($key == 1 or $LETTERS_DATA[$key]['PROCESS'] != $LETTERS_DATA[$key - 1]['PROCESS']) {
        $VALIDATION_LETTERS_DATA_DETAILS .= '<table style="border: 1px solid black; border-collapse: collapse;"><tbody>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<tr>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: left;"><b>' . $LETTER_DATA['PROCESS'] . '</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>STATUS</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>DATE</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '</tr>';
    }
    // Datos del proceso
    $BRAND = $LETTER_DATA['BRAND'] ? ' - ' . $LETTER_DATA['BRAND'] : '';
    $MODEL = $LETTER_DATA['MODEL'] ? ' - ' . $LETTER_DATA['MODEL'] : '';
    $BBN = $LETTER_DATA['BBN'] ? ' - ' . $LETTER_DATA['BBN'] : '';
    $THERMAL_TREATMENT = $LETTER_DATA['THERMAL_TREATMENT'] ? ' - ' . $LETTER_DATA['THERMAL_TREATMENT'] : '';
    $PRODUCT_NAME = $LETTER_DATA['PRODUCT_NAME'] ? ' - ' . $LETTER_DATA['PRODUCT_NAME'] : '';
    $LETTER_LINE = $LETTER_DATA['LINE'] ? ' - L' . $LETTER_DATA['LINE'] : '';
    $DESCRIPTION = $LETTER_DATA['DESCRIPTION'] ? '<br>Description: ' . $LETTER_DATA['DESCRIPTION'] : '';
    $S_PERFORMANCE_COMMENTS_KO = $LETTER_DATA['S_PERFORMANCE_COMMENTS_KO'] ? '<br>Comments by KO: ' . $LETTER_DATA['S_PERFORMANCE_COMMENTS_KO'] : '';
    
    if ($LETTER_DATA['STATUS'] == 'PERFORMANCE APPROVED') {
        $STATUS = 'Approved';
    } else if ($LETTER_DATA['STATUS'] == 'Performance conditioned') {
        $STATUS = 'Conditioned';
    } else {
        $STATUS = $LETTER_DATA['STATUS'];
    }
    
    $PACKAGE_TYPE = '';
    if((strpos($LETTER_DATA['TYPE'], "ASEBI"))=== false) { 
        $PACKAGE_TYPE = $LETTER_DATA['PACKAGE_TYPE'] ? ' - ' . $LETTER_DATA['PACKAGE_TYPE'] : '';
    }
    
    $VALIDATION_LETTERS_DATA_DETAILS .= '<tr>';
    $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: left;">' . $LETTER_DATA['TYPE'] . $PACKAGE_TYPE . $LETTER_LINE . $BRAND . $MODEL . $BBN . $THERMAL_TREATMENT . $PRODUCT_NAME . $DESCRIPTION . $S_PERFORMANCE_COMMENTS_KO . '</td>';
    $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;">' . $STATUS . '</td>';
    $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;">' . $LETTER_DATA['APPROVAL_DATE'] . '</td>';
    $VALIDATION_LETTERS_DATA_DETAILS .='</tr>';

    if((strpos($LETTER_DATA['TYPE'], "ASEBI"))!== false) {
        $VALIDATION_LETTERS_DATA_DETAILS .= '</tbody></table>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<p></p>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<p>See below the details of packages authorized for this ASEBI.</p>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<table style="border: 1px solid black; border-collapse: collapse;"><tbody>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<tr>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: left;"><b>PRODUCT NAME</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>VOLUME (ml)</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>PACKAGE TYPE</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>SPEED (bpm)</b></td>';
        $VALIDATION_LETTERS_DATA_DETAILS .= '</tr>';
        foreach ($ASEBI_DETAIL as $ASEBI) {
            if($ASEBI['CASE_NUMBER'] == $LETTER_DATA['CASE_NUMBER']) {   
                $ASEBI_PACKAGE_TYPE = $ASEBI['PACKAGE_TYPE'] == 1 ? 'REF PET' : 'RGB';
                $VALIDATION_LETTERS_DATA_DETAILS .= '<tr>';
                $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: left;">' . $ASEBI['PRODUCT_NAME']  . '</td>';
                $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;">' . $ASEBI['VOLUME']  . '</td>';
                $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;">' . $ASEBI_PACKAGE_TYPE  . '</td>';
                $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;">' . $ASEBI['SPEED']  . '</td>';
                $VALIDATION_LETTERS_DATA_DETAILS .= '</tr>';
            }
        }
        if($key == 1 or $LETTERS_DATA[$key]['PROCESS'] == $LETTERS_DATA[$key + 1]['PROCESS']) {
            $VALIDATION_LETTERS_DATA_DETAILS .= '</tbody></table>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<p></p><p></p>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<table style="border: 1px solid black; border-collapse: collapse;"><tbody>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<tr>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: left;"><b>' . $LETTER_DATA['PROCESS'] . '</b></td>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>STATUS</b></td>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '<td style="border: 1px solid black; border-collapse: collapse; text-align: center;"><b>DATE</b></td>';
            $VALIDATION_LETTERS_DATA_DETAILS .= '</tr>';
        }
    }
}
$VALIDATION_LETTERS_DATA_DETAILS .= '</tbody></table>';
@=VALIDATION_LETTERS_DATA_DETAILS = $VALIDATION_LETTERS_DATA_DETAILS;
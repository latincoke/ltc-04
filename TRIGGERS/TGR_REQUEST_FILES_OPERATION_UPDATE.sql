$CASE_NUMBER    = @@I_FOLIO;
$A_FILES_OLD = array();
$A_FILES_OLD_LIST = '';
// Obtener URL de Files viejos para posteriormente buscarlo en la tabla PMT_PRP_REQUEST_DOCUMENTS_OPERATION
foreach (@@F_OPERATION_CALIBRATION_RECORDS_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_CALIBRATION_RECORDS_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_MICROPERFORMANCE_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_MICROPERFORMANCE_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_CIP_VALIDATION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_CIP_VALIDATION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_OPERATIONAL_TEST_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_OPERATIONAL_TEST_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_THIRD_PARTY_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_THIRD_PARTY_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_RINSER_VALIDATION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_RINSER_VALIDATION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_PASTEURIZER_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_PASTEURIZER_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_OTHER_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_OTHER_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_FACILITY_DESIGN_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_FACILITY_DESIGN_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_GMP_EVALUATION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_GMP_EVALUATION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_MINIMUM_REQUIREMENTS_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_MINIMUM_REQUIREMENTS_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_RISK_ASSESSMENT_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_RISK_ASSESSMENT_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_CAP_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_CAP_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_OTHERS_RELEVANT_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_OTHERS_RELEVANT_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_ASEBI_VALIDATION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_ASEBI_VALIDATION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_WASHER_EFECTIVENESS_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_WASHER_EFECTIVENESS_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_STERILE_CONDITION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_STERILE_CONDITION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_ANALYST_CERTIFICATION_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_ANALYST_CERTIFICATION_URL_LABEL'] . "'");
}
foreach (@@F_OPERATION_PROCESS_AUTHORITY_HEAT_GRID as $key => $DOCUMENTO) {
    array_push($A_FILES_OLD, "'" . $DOCUMENTO['F_OPERATION_PROCESS_AUTHORITY_HEAT_URL_LABEL'] . "'");
}
$A_FILES_OLD_LIST = join(', ', $A_FILES_OLD);
if($A_FILES_OLD_LIST != '') {
    // Guarda los archivos eliminados en PRP_REQUEST_FILES_LOG
    $query =
        "INSERT INTO PMT_PRP_REQUEST_FILES_LOG
        (
            ID,
            CASE_NUMBER,
            STAGE,
            DATE_EVENT,
            FILE_EVENT,
            FILENAME,
            FILE_TYPE_ID,
            STATUS
        )
        SELECT
            NULL,
            '" . $CASE_NUMBER . "',
            'Operational',
            Now(),
            'Deleted',
            APP_DOCUMENT_FILENAME,
            APP_DOCUMENT_FIELDNAME,
            1
        FROM
            PMT_PRP_REQUEST_DOCUMENTS_OPERATION
        WHERE
            CASE_NUMBER = '" . $CASE_NUMBER . "'
        AND
            LINK NOT IN (" . $A_FILES_OLD_LIST . ")";
    executeQuery($query);
    // Guarda los archivos en PMT_PRP_REQUEST_DOCUMENTS_OPERATION
    $query =
        "INSERT INTO PMT_PRP_REQUEST_DOCUMENTS_OPERATION
        (
            ID,
            CASE_NUMBER,
            APP_UID,
            APP_DOCUMENT_UID,
            APP_DOCUMENT_FILENAME,
            EXTENTION,
            APP_DOCUMENT_FIELDNAME,
            LINK
        )
        SELECT
            NULL,
            '" . $CASE_NUMBER . "',
            '" . @@APPLICATION . "',
            APP_DOCUMENT_UID,
            APP_DOCUMENT_FILENAME,
            EXTENTION,
            APP_DOCUMENT_FIELDNAME,
            LINK
        FROM
            PMT_PRP_REQUEST_DOCUMENTS_OPERATION
        WHERE
            CASE_NUMBER = '" . $CASE_NUMBER . "'
        AND
            LINK IN (" . $A_FILES_OLD_LIST . ")";
    executeQuery($query);
} else {
    // Guarda los archivos eliminados en PRP_REQUEST_FILES_LOG
    $query =
        "INSERT INTO PMT_PRP_REQUEST_FILES_LOG
        (
            ID,
            CASE_NUMBER,
            STAGE,
            DATE_EVENT,
            FILE_EVENT,
            FILENAME,
            FILE_TYPE_ID,
            STATUS
        )
        SELECT
            NULL,
            '" . $CASE_NUMBER . "',
            'Operational',
            Now(),
            'Deleted',
            APP_DOCUMENT_FILENAME,
            APP_DOCUMENT_FIELDNAME,
            1
        FROM
            PMT_PRP_REQUEST_DOCUMENTS_OPERATION
        WHERE
            CASE_NUMBER = '" . $CASE_NUMBER . "'";
    executeQuery($query);
}
executeQuery("DELETE FROM PMT_PRP_REQUEST_DOCUMENTS_OPERATION WHERE CASE_NUMBER = '" . $CASE_NUMBER . "' AND APP_UID <> '" . @@APPLICATION ."'");

$DOCUMENTOS = executeQuery(
    "SELECT 
        APP_DOC_UID,
        APP_DOC_FILENAME,
        TRIM(REPLACE(RIGHT(APP_DOC_FILENAME,4),'.','')) AS EXTENTION,
        APP_DOC_FIELDNAME,
        CONCAT (
            '/cases/cases_ShowDocument?a=',
            APP_DOC_UID
        ) AS LINK
    FROM
        APP_DOCUMENT
    WHERE
        APP_UID = '" . @@APPLICATION . "' AND
        APP_DOC_STATUS = 'ACTIVE'"
);

$LINK=((G::is_https())? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/sys" . @@SYS_SYS . "/" . @@SYS_LANG . "/" . @@SYS_SKIN;

foreach ($DOCUMENTOS as $key => $DOCUMENTO) {
    $query = 
        "INSERT INTO PMT_PRP_REQUEST_DOCUMENTS_OPERATION
        (
            ID,
            CASE_NUMBER,
            APP_UID,
            APP_DOCUMENT_UID,
            APP_DOCUMENT_FILENAME,
            EXTENTION,
            APP_DOCUMENT_FIELDNAME,
            LINK
        ) 
        VALUES 
        (
            NULL,
            '$CASE_NUMBER',
            '" . @@APPLICATION . "',
            '" . $DOCUMENTO[APP_DOC_UID] . "',
            '" . $DOCUMENTO[APP_DOC_FILENAME] . "',
            '" . $DOCUMENTO[EXTENTION] . "',
            '" . $DOCUMENTO[APP_DOC_FIELDNAME] . "',
            '" . $LINK . $DOCUMENTO[LINK] ."'
        )";		
    executeQuery($query);
    // Guarda los archivos agregados en PRP_REQUEST_FILES_LOG
    $query = 
        "INSERT INTO PMT_PRP_REQUEST_FILES_LOG
        (
            ID,
            CASE_NUMBER,
            STAGE,
            DATE_EVENT,
            FILE_EVENT,
            FILENAME,
            FILE_TYPE_ID,
            STATUS
        )
        VALUES 
        (
            NULL,
            '$CASE_NUMBER',
            'Operational',
            Now(),
            'Upload',
            '" . $DOCUMENTO[APP_DOC_FILENAME] . "',
            '" . $DOCUMENTO[APP_DOC_FIELDNAME] . "',
            1
        )";		
    executeQuery($query);
}
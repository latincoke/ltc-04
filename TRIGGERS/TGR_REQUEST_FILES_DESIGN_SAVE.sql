$CASE_NUMBER    = @@APP_NUMBER;
$DOCUMENTOS = executeQuery(
    "SELECT 
        APP_DOC_UID,
        APP_DOC_FILENAME,
        TRIM(REPLACE(RIGHT(APP_DOC_FILENAME,4),'.','')) AS EXTENTION,
        APP_DOC_FIELDNAME,
        CONCAT (
            '/cases/cases_ShowDocument?a=',
            APP_DOC_UID
        ) AS LINK
    FROM
        APP_DOCUMENT
    WHERE
        APP_UID = '" . @@APPLICATION . "' AND
        APP_DOC_STATUS = 'ACTIVE'"
);

$LINK=((G::is_https())? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/sys" . @@SYS_SYS . "/" . @@SYS_LANG . "/" . @@SYS_SKIN;

foreach ($DOCUMENTOS as $key => $DOCUMENTO) {
    $query = 
        "INSERT INTO PMT_PRP_REQUEST_DOCUMENTS_DESIGN
        (
            ID,
            CASE_NUMBER,
            APP_UID,
            APP_DOCUMENT_UID,
            APP_DOCUMENT_FILENAME,
            EXTENTION,
            APP_DOCUMENT_FIELDNAME,
            LINK
        ) 
        VALUES 
        (
            NULL,
            '$CASE_NUMBER',
            '" . @@APPLICATION . "',
            '" . $DOCUMENTO[APP_DOC_UID] . "',
            '" . $DOCUMENTO[APP_DOC_FILENAME] . "',
            '" . $DOCUMENTO[EXTENTION] . "',
            '" . $DOCUMENTO[APP_DOC_FIELDNAME] . "',
            '" . $LINK . $DOCUMENTO[LINK] ."'
        )";		
    executeQuery($query);
    // Guarda los archivos agregados en PRP_REQUEST_FILES_LOG
    $query = 
        "INSERT INTO PMT_PRP_REQUEST_FILES_LOG
        (
            ID,
            CASE_NUMBER,
            STAGE,
            DATE_EVENT,
            FILE_EVENT,
            FILENAME,
            FILE_TYPE_ID,
            STATUS
        )
        VALUES 
        (
            NULL,
            '$CASE_NUMBER',
            'Design',
            Now(),
            'Upload',
            '" . $DOCUMENTO[APP_DOC_FILENAME] . "',
            '" . $DOCUMENTO[APP_DOC_FIELDNAME] . "',
            1
        )";		
    executeQuery($query);
}
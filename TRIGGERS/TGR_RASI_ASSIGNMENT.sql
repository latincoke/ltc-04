//AUTOR: BSTI 
//www.BSTI.com.mx

$designatedTom = @@S_TOM_DESIGNATED;
$validationType = @@S_VALIDATION_TYPE;

//GET GROUPS IDS	
  $groupName = "04PJ ENGINEERING";
  $groupEngineering = PMFGetGroupUID($groupName);

  $groupName = "04PJ QSE DIRECTOR";
  $groupQseDirector = PMFGetGroupUID($groupName);

  $groupName = "04PJ QUALITY";
  $groupQuality = PMFGetGroupUID($groupName);

  $groupName = "04PJ Tom";
  $groupTom = PMFGetGroupUID($groupName);

//ASSIGN GROUP TO VARIABLE
	switch ($validationType) {
		case 2:
		case 3:
			@@S_GROUP_DESIGN = $designatedTom ;
			@@S_GROUP_INSTALLATION = $designatedTom;
			@@S_GROUP_OPERATION = $designatedTom;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			@@S_GROUP_QSE = $groupQuality;
			break;
		case 4:
		case 6:
		case 8:
			@@S_GROUP_DESIGN = $groupEngineering;
			@@S_GROUP_INSTALLATION = $groupQuality;
			@@S_GROUP_OPERATION = $groupQuality;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			break;			
		case 5:
			@@S_GROUP_DESIGN = $designatedTom;
			@@S_GROUP_INSTALLATION = $designatedTom;
			@@S_GROUP_OPERATION = $groupQuality;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			break;
		case 7:
			@@S_GROUP_DESIGN = $designatedTom;
			@@S_GROUP_INSTALLATION = $designatedTom;
			@@S_GROUP_OPERATION = $designatedTom;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			@@S_GROUP_QSE = $groupQuality;
			break;
		case 1:
			@@S_GROUP_DESIGN = $groupEngineering;
			@@S_GROUP_INSTALLATION = $groupEngineering;
			@@S_GROUP_OPERATION = $groupQuality;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			break;			
		case 9:
			@@S_GROUP_DESIGN = $groupQuality;
			@@S_GROUP_INSTALLATION = $groupQuality;
			@@S_GROUP_OPERATION = $groupQuality;
			@@S_GROUP_PERFORMANCE = $groupQuality;
			break;
		case 10:
			@@S_GROUP_DESIGN = $designatedTom;
			@@S_GROUP_INSTALLATION = $designatedTom;
			@@S_GROUP_OPERATION = $designatedTom;
			@@S_GROUP_PERFORMANCE = $designatedTom;
			break;			
	}
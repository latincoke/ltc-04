$liga = ((G::is_https()) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . "/sys" . @@SYS_SYS . "/" . @@SYS_LANG . "/" . @@SYS_SKIN;
$wsdl = $liga . '/services/wsdl2';

//lookup the user assigned to the next task in the case
$caseId = @@APPLICATION;
$appNumber = @@APP_NUMBER;
$query = "SELECT
            AD.DEL_INDEX,
            AD.APP_UID,
            U.USR_USERNAME
        FROM
            APP_DELEGATION AD,
            USERS U 
        WHERE
            AD.APP_NUMBER='$appNumber' AND
            AD.DEL_INDEX=(
                SELECT 
                    MAX(DEL_INDEX)
                FROM 
                    APP_DELEGATION
                WHERE
                    APP_NUMBER='$appNumber') AND
            AD.USR_UID=U.USR_UID";

$result = executeQuery($query);

$nextUser = $result[1]['USR_USERNAME'];
$nextIndex = $result[1]['DEL_INDEX'];
$newCaseId = $result[1]['APP_UID'];


//lookup the md5 hash for the password
$result = executeQuery("SELECT USR_PASSWORD FROM USERS WHERE USR_USERNAME='$nextUser'");
$nextPass = 'md5:' . $result[1]['USR_PASSWORD'];

$client = new SoapClient($wsdl);
$params = array(array('userid' => $nextUser, 'password' => $nextPass));
$result = $client->__SoapCall('login', $params);

if ($result->status_code == 0)
    $sessionId = $result->message;
else
    die("Unable to connect to ProcessMaker.\nError Message: {$result->message}");
 
$params = array(array(
   'sessionId'=> $sessionId,
   'caseId'    => $newCaseId,
   'delIndex'  => $nextIndex
));

$result = $client->__SoapCall('routeCase', $params);
if ($result->status_code != 0)
    die("Error routing case: {$result->message}\n");
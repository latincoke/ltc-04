$to = @@S_REQUESTER_EMAIL;
$cco = @@COO_RASI_USERS;
$cco .= @@S_PLANT_MANAGER_EMAIL . ', ';
$cco .= @@S_TOM_EMAIL . ', ';
$cco .= @@S_MANAGER_EMAIL;
$APP_UID = @@APPLICATION;
$aAttachFiles = array();

$outDocQuery = "
    SELECT
        APP_DOC_UID,
        DOC_VERSION,
        APP_DOC_FILENAME AS FILENAME,
        DOC_UID,
        APP_UID
    FROM
        APP_DOCUMENT
    WHERE
        APP_UID = '$APP_UID' AND
        APP_DOC_TYPE = 'OUTPUT'
        AND APP_DOC_STATUS = 'ACTIVE'
    ORDER BY
        APP_DOC_INDEX DESC
    LIMIT 1";
$outDoc = executeQuery($outDocQuery);

if (!empty($outDoc)) {
   $g = new G();
   $path = PATH_DOCUMENT . $g->getPathFromUID($APP_UID) . PATH_SEP . 'outdocs'. PATH_SEP .
      $outDoc[1]['APP_DOC_UID'] . '_' . $outDoc[1]['DOC_VERSION'];
   $filename = $outDoc[1]['FILENAME'];
   $aAttachFiles[$filename . '.pdf'] = $path . '.pdf';  //remove if not generating a PDF file
}
//SEND NOTIFICATIONS
PMFSendMessage(@@APPLICATION, 'processmaker@coca-cola.com', $to, $cco,'', 'PERFORMANCE APPROVED|'.@@S_CASE_TITLE_LABEL, 'TP_PERFORMANCE_APPROVED.html', array(), $aAttachFiles);
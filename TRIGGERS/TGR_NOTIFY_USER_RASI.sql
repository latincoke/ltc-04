$USERS=executeQuery(
"(SELECT
        CONCAT(
            U.USR_FIRSTNAME,
            ' ',
            U.USR_LASTNAME,
            '  <',
            U.USR_EMAIL,
            '> '
        ) as CCO
    FROM
        USERS U
    INNER JOIN
        PMT_PJ_CASES_RASI_SETUP RASI
    ON
        U.USR_UID = RASI.DESIGN_CHECK_USR
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'
) UNION (SELECT
        CONCAT(
            U.USR_FIRSTNAME,
            ' ',
            U.USR_LASTNAME,
            '  <',
            U.USR_EMAIL,
            '> '
        ) as CCO
    FROM
        USERS U
    INNER JOIN
        PMT_PJ_CASES_RASI_SETUP RASI
    ON
        U.USR_UID = RASI.INSTALLATION_CHECK_USR
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'
) UNION (SELECT
        CONCAT(
            U.USR_FIRSTNAME,
            ' ',
            U.USR_LASTNAME,
            '  <',
            U.USR_EMAIL,
            '> '
        ) as CCO
    FROM
        USERS U
    INNER JOIN
        PMT_PJ_CASES_RASI_SETUP RASI
    ON
        U.USR_UID = RASI.OPERATION_CHECK_USR
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'
) UNION (SELECT
        CONCAT(
            U.USR_FIRSTNAME,
            ' ',
            U.USR_LASTNAME,
            '  <',
            U.USR_EMAIL,
            '> '
        ) as CCO
    FROM
        USERS U
    INNER JOIN
        PMT_PJ_CASES_RASI_SETUP RASI
    ON
        U.USR_UID = RASI.PERFORMANCE_CHECK_USR
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'
) UNION (SELECT
        CONCAT(
            U.USR_FIRSTNAME,
            ' ',
            U.USR_LASTNAME,
            '  <',
            U.USR_EMAIL,
            '> '
        ) as CCO
    FROM
        USERS U
    INNER JOIN
        PMT_PJ_CASES_RASI_SETUP RASI
    ON
        U.USR_UID = RASI.QSE_CHECK_USR
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'
)"
);
foreach ($USERS as $key => $user) {
    $COO_RASI_USERS .= $user[CCO] . ', ';
}
@=COO_RASI_USERS = $COO_RASI_USERS;
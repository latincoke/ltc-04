$S_PROJECT_TYPE = @@S_PROJECT_TYPE;
$S_TYPE = @@S_TYPE;
$S_VALIDATION_TYPE = @@S_VALIDATION_TYPE;

$REQUEST=executeQuery(
    "SELECT 
        S_PROJECT_TYPE,
        S_TYPE,
        S_VALIDATION_TYPE
    FROM
        PMT_PRP_REQUEST_DETAIL
    WHERE
        CASE_NUMBER = '" . @@APP_NUMBER . "'"
);
if(
    $S_PROJECT_TYPE != $REQUEST[1]['S_PROJECT_TYPE'] OR
    $S_TYPE != $REQUEST[1]['S_TYPE'] OR
    $S_VALIDATION_TYPE != $REQUEST[1]['S_VALIDATION_TYPE']
)   {
    $query =
        "INSERT INTO PMT_PRP_REQUEST_FILES_LOG
        (
            ID,
            CASE_NUMBER,
            STAGE,
            DATE_EVENT,
            FILE_EVENT,
            FILENAME,
            FILE_TYPE_ID,
            STATUS
        )
        SELECT
            NULL,
            '" . @@APP_NUMBER . "',
            'Design',
            Now(),
            'Deleted',
            APP_DOCUMENT_FILENAME,
            APP_DOCUMENT_FIELDNAME,
            1
        FROM
            PMT_PRP_REQUEST_DOCUMENTS_DESIGN
        WHERE
            CASE_NUMBER = '" . @@APP_NUMBER . "'";
    executeQuery($query);
    executeQuery("DELETE FROM PMT_PRP_REQUEST_DOCUMENTS_DESIGN WHERE CASE_NUMBER = '" . @@APP_NUMBER . "'");
}
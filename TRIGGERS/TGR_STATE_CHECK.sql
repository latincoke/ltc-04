$CASE_NUMBER = @@APP_NUMBER;
$I_REQUEST_APPROVAL = @@I_REQUEST_APPROVAL;
$S_GROUP_QSE = @@S_GROUP_QSE;
$REQUEST=executeQuery(
    "SELECT
        VALIDATION_STAGE
    FROM
        PMT_PRP_REQUEST
    WHERE
        CASE_NUMBER = '" . $CASE_NUMBER . "'
    ORDER BY
        ID DESC
    LIMIT 1
");
$VALIDATION_STAGE = $REQUEST[1]['VALIDATION_STAGE'];
switch($VALIDATION_STAGE){
    case "Design":
		if(@=I_USE_INSTALLATION == 1) {
        	@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Installation" : "Design";
		} else {
			if(@=I_USE_OPERATIONAL == 1) {
				@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Operational" : "Design";
			} else {
				if($S_GROUP_QSE != '') {
					@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "QSE validation" : "Design";
				} else {
					@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Performance" : "Design";
				}
			}
		}	
        @=STATE             = $I_REQUEST_APPROVAL == 1 ? "Design approved" : "Design rejected";
        break;
    case "Installation":
		if(@=I_USE_OPERATIONAL == 1) {
			@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Operational" : "Installation";
		} else {
			if($S_GROUP_QSE != '') {
				@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "QSE validation" : "Installation";
			} else {
				@=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Performance" : "Installation";
			}
		}		
        @=STATE             = $I_REQUEST_APPROVAL == 1 ? "Installation approved" : "Installation rejected";
        break;
    case "Operational":
        if($I_REQUEST_APPROVAL == 1) {
            if($S_GROUP_QSE != '') {
                @=VALIDATION_STAGE = "QSE validation";
            } else {
                @=VALIDATION_STAGE = "Performance";
            }
        } else {
            @=VALIDATION_STAGE = "Operational";
        }
        @=STATE             = $I_REQUEST_APPROVAL == 1 ? "Operational approved" : "Operational rejected";
        break;
    case "QSE validation":
        @=VALIDATION_STAGE  = $I_REQUEST_APPROVAL == 1 ? "Performance" : "Operational";
        @=STATE             = $I_REQUEST_APPROVAL == 1 ? "QSE validation approved" : "QSE validation rejected";
        break;
    case "Performance":
        @=VALIDATION_STAGE = "Performance";
        if($I_REQUEST_APPROVAL == 1) {
            @=STATE = "Approved";
        } else if($I_REQUEST_APPROVAL == 2) {
            @=STATE = "Performance rejected";
        } else if($I_REQUEST_APPROVAL == 3) {
            @=STATE = "Performance conditioned";
        }
        break;
}
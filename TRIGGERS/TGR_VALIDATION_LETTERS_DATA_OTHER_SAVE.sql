$S_OTHER_LETTER = @@S_OTHER_LETTER;
$S_OTHER_STATUS_LETTER = 'Approved';
$S_OTHER_DESCRIPTION = @@S_OTHER_DESCRIPTION;
$APPROVAL_DATE = @@S_OTHER_MONTH . ' ' . @@S_OTHER_YEAR;
$S_PLANT_ID = @@S_PLANT_ID;
$S_PLANT_NAME = @@S_PLANT_NAME;
$S_COUNTRY = @@S_COUNTRY;
$CASE_NUMBER = @@APP_NUMBER;

if($S_OTHER_LETTER != '' || $S_OTHER_DESCRIPTION != '' ) {
    $query =
    "INSERT INTO PMT_PRP_VALIDATION_LETTERS_DATA
    (
        ID,
        DESCRIPTION,
        STATUS,
        COMMENTS,
        APPROVAL_DATE,
        PLANT_ID,
        PLANT_NAME,
        DATE_REQUEST,
        COUNTRY,
        CASE_NUMBER,
        PROCESS,
        TYPE,
        TYPE_ID
    ) 
    VALUES
    (
        NULL,
        '$S_OTHER_LETTER',
        '$S_OTHER_STATUS_LETTER',
        '$S_OTHER_DESCRIPTION',
        '$APPROVAL_DATE',
        '$S_PLANT_ID',
        '$S_PLANT_NAME',
        NOW(),
        '$S_COUNTRY',
        '$CASE_NUMBER',
        'Other approvals',
        'Other approvals',
        0
    )";
    executeQuery($query);
}
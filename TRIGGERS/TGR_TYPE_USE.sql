@=I_USE_INSTALLATION    = 1;
@=I_USE_OPERATIONAL     = 1;

$Net_content_inspector      =   27;
$Closure_application_inspector  =   28;
$Empty_Bottle_Inspector_ASEBI   =   29;
$Empty_Bottle_Inspector_Sniffer =   30;
$Waste_Water_Treatment      =   58;
$Other_processes_equipment  =   60;

$Heat_treatment_equivalent  =   51;
$Other                      =    61;
$Rinser_Removal             =   64;
$Changes_in_Water           =    69;
$Quarantine_Removal         =   62;
$Extension_of_commercial_runs    =   63;
$Finished_Product           = 65;
$Ingredient_Monitoring      =   66;
$Packaged_Monitoring        = 67;
$Color_Specification        =  70;
$Ingredients_Specification  =  71;
$Sampling_Plan_Changes      =   72;
$Heat_treated_products_with_limit_recovered_solids_more_than_10 =   49;
$Thermal_manufacturing_Beverage_recirculation_and_line_stoppage_process =   50;

switch (@@S_TYPE) {
    case $Net_content_inspector:
    case $Closure_application_inspector:
    case $Empty_Bottle_Inspector_ASEBI:
    case $Empty_Bottle_Inspector_Sniffer:
    case $Waste_Water_Treatment:
    case $Other_processes_equipment:
    case $Heat_treatment_equivalent:
    case $Other:
    case $Rinser_Removal:
    case $Changes_in_Water:
    case $Quarantine_Removal:
    case $Extension_of_commercial_runs:
    case $Finished_Product:
    case $Ingredient_Monitoring:
    case $Packaged_Monitoring:
    case $Color_Specification:
    case $Ingredients_Specification:
    case $Sampling_Plan_Changes:
    case $Heat_treated_products_with_limit_recovered_solids_more_than_10:
    case $Thermal_manufacturing_Beverage_recirculation_and_line_stoppage_process:
        @=I_USE_INSTALLATION = 0;
        break;
}

switch (@@S_TYPE) {
    case $Net_content_inspector:
    case $Closure_application_inspector:
    case $Empty_Bottle_Inspector_ASEBI:
    case $Quarantine_Removal:
    case $Extension_of_commercial_runs:
    case $Finished_Product:
    case $Ingredient_Monitoring:
    case $Packaged_Monitoring:
    case $Color_Specification:
    case $Ingredients_Specification:
    case $Sampling_Plan_Changes:
        @=I_USE_OPERATIONAL = 0;
        break;
}
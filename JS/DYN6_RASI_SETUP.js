/* BSTI 2020 */
/* www.bsti.com.mx */

//CSS ATRIBUTES FOR COMPONENTES
$('.pmdynaform-disabled-textarea > input[type="text"], textarea').css('background-color', '#FFFF99');
$('.pmdynaform-edit-text, .pmdynaform-edit-file').css('background-color', '#FFFF99');
$('.pmdynaform-field-text > input[type="text"],textarea[disabled], text').css('background-color', '#F5F5F5');
$('.pmdynaform-field-dropdown').css('background-color','#FFFF99');
$('.pmdynaform-disabled-dropdown').css('background-color','#FFFFFF');
$('.pmdynaform-field-grid').css('background', '#FFFF99');
$('.pmdynaform-disabled-grid').css('background-color','#FFFFFF');
$('.pmdynaform-field-submit').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-field-submit').find('Button').css({'color':'white'});
$('.pmdynaform-label-subtitle').css('color', '#EB2D2F');
$('.pmdynaform-label-subtitle').css('font-weight', 'bold');
$('#F_FILE_OTHER').css('background-color','#FFFF99');
$('.pmdynaform-multiplefile-control').find('Button').css({'color':'white'});
$('.pmdynaform-multiplefile-control').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-file-control').find('Button').css({'color':'white'});
$('.pmdynaform-file-control').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-disabled-multipleFile').find('Button').css({'color':'#808080'});
$('.pmdynaform-disabled-multipleFile').find('Button').css({'background-color':'#F5F5F5'});

$("#PN_TITLE").css({
  "border-style": "none",
  "border-bottom": "6px solid",
  "border-bottom-color": "#EB2D2F"
});
$("#S_ENGENEERING_USER").hide();
$("#S_QUALITY_USER").hide();
select_value();
$("#S_TYPE_RASI_SETUP").setOnchange(function() {
    select_value();
});

function select_value(){
    $('#S_ENGENEERING_CHECK_USER').setValue(document.getElementById('form[S_ENGENEERING_USER]').value);
    $('#S_QUALITY_CHECK_USER').setValue(document.getElementById('form[S_QUALITY_USER]').value);
}
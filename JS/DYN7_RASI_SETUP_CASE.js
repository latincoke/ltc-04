/* BSTI 2020 */
/* www.bsti.com.mx */

//CSS ATRIBUTES FOR COMPONENTES
$('.pmdynaform-disabled-textarea > input[type="text"], textarea').css('background-color', '#FFFF99');
$('.pmdynaform-edit-text, .pmdynaform-edit-file').css('background-color', '#FFFF99');
$('.pmdynaform-field-text > input[type="text"],textarea[disabled], text').css('background-color', '#F5F5F5');
$('.pmdynaform-field-dropdown').css('background-color','#FFFF99');
$('.pmdynaform-disabled-dropdown').css('background-color','#FFFFFF');
$('.pmdynaform-field-grid').css('background', '#FFFF99');
$('.pmdynaform-disabled-grid').css('background-color','#FFFFFF');
$('.pmdynaform-field-submit').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-field-submit').find('Button').css({'color':'white'});
$('.pmdynaform-label-subtitle').css('color', '#EB2D2F');
$('.pmdynaform-label-subtitle').css('font-weight', 'bold');
$('#F_FILE_OTHER').css('background-color','#FFFF99');
$('.pmdynaform-multiplefile-control').find('Button').css({'color':'white'});
$('.pmdynaform-multiplefile-control').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-file-control').find('Button').css({'color':'white'});
$('.pmdynaform-file-control').find('Button').css({'background-color':'#F05C5E'});
$('.pmdynaform-disabled-multipleFile').find('Button').css({'color':'#808080'});
$('.pmdynaform-disabled-multipleFile').find('Button').css({'background-color':'#F5F5F5'});

$("#PN_TITLE").css({
  "border-style": "none",
  "border-bottom": "6px solid",
  "border-bottom-color": "#EB2D2F"
});
$("#S_DESIGN_CHECK_USER").hide();
$("#S_INSTALLATION_CHECK_USER").hide();
$("#S_OPERATION_CHECK_USER").hide();
$("#S_QSE_CHECK_USER").hide();
$("#S_PERFORMANCE_CHECK_USER").hide();
$("#S_VALIDATION_TYPE").hide();
not_found();
$("#not_found").hide();

$("#S_CASE_NUMBER").setOnchange(function() {
    if(document.getElementById('form[S_DESIGN_CHECK_USER]').value == "") {
      not_found();
    } else {
      is_found();
      $('#S_GROUP_DESIGN').setValue(document.getElementById('form[S_DESIGN_CHECK_USER]').value);
      $('#S_GROUP_INSTALLATION').setValue(document.getElementById('form[S_INSTALLATION_CHECK_USER]').value);
      $('#S_GROUP_OPERATION').setValue(document.getElementById('form[S_OPERATION_CHECK_USER]').value);
      $('#S_GROUP_QSE').setValue(document.getElementById('form[S_QSE_CHECK_USER]').value);
      $('#S_GROUP_PERFORMANCE').setValue(document.getElementById('form[S_PERFORMANCE_CHECK_USER]').value);
      if(
        document.getElementById('form[S_VALIDATION_TYPE]').value == 2 ||
        document.getElementById('form[S_VALIDATION_TYPE]').value == 3 ||
        document.getElementById('form[S_VALIDATION_TYPE]').value == 7
      ){
        $("#S_GROUP_QSE").show();
      } else {
        $("#S_GROUP_QSE").hide();
      }
    }
});

function is_found() {
  $("#not_found").hide();
  $('#S_GROUP_DESIGN').show();
  $('#S_GROUP_INSTALLATION').show();
  $('#S_GROUP_OPERATION').show();
  $('#S_GROUP_QSE').show();
  $('#S_GROUP_PERFORMANCE').show();
  $('#label0000000001').show();
  $('#S_INFORMATION_CASE').show();
  $('#S_INFORMATION_CASE_DATA').show();
  $('#submit0000000001').show();
}
function not_found() {
  $("#not_found").show();
  $('#S_GROUP_DESIGN').hide();
  $('#S_GROUP_INSTALLATION').hide();
  $('#S_GROUP_OPERATION').hide();
  $('#S_GROUP_QSE').hide();
  $('#S_GROUP_PERFORMANCE').hide();
  $('#label0000000001').hide();
  $('#S_INFORMATION_CASE').hide();
  $('#S_INFORMATION_CASE_DATA').hide();
  $('#submit0000000001').hide();
}
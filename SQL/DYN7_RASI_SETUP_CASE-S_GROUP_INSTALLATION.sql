/* INSTALLATION */
SELECT '0', '-Select an option-' 
    UNION
SELECT
    U.USR_UID,
    CONCAT(
        U.USR_FIRSTNAME,
        ' ',
        U.USR_LASTNAME,
        ' --- ',
        U.USR_EMAIL
    )
FROM
    USERS U
LEFT JOIN
    GROUP_USER GU ON U.USR_UID = GU.USR_UID
LEFT JOIN
    CONTENT C ON GU.GRP_UID = C.CON_ID
WHERE
    ((@@S_VALIDATION_TYPE = 3 OR @@S_VALIDATION_TYPE = 2 OR @@S_VALIDATION_TYPE = 5 OR @@S_VALIDATION_TYPE = 7 OR @@S_VALIDATION_TYPE = 10) AND C.CON_VALUE = '04PJ TOM')
    OR
    ((@@S_VALIDATION_TYPE = 1) AND C.CON_VALUE = '04PJ ENGINEERING')
    OR
    ((@@S_VALIDATION_TYPE = 4 OR @@S_VALIDATION_TYPE = 6 OR @@S_VALIDATION_TYPE = 8 OR @@S_VALIDATION_TYPE = 9) AND C.CON_VALUE = '04PJ QUALITY')
    ;
/*
01 New Facility
02 Conversion in existing Line (CSD to Stills)
03 New CSD line
04 New Production Line Stills, Plant Based & Dairy
05 New Beverage
06 New Process
07 Equipment Change
08 New Technology		
09 Process Change Tipo 1
10 Process Change Tipo 2
*/
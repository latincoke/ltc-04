$query = 
"INSERT INTO PMT_PRP_VALIDATION_LETTERS_DATA 
    (ID,CASE_NUMBER,DATE_REQUEST,COUNTRY,PLANT_NAME,PLANT_ID,REQUESTOR_NAME,REQUESTOR_EMAIL,QUALITY_MANAGER,QUALITY_MANAGER_EMAIL,PLANT_MANAGER,PLANT_MANAGER_EMAIL,TOM_NAME,TOM_EMAIL,CPK,PROCESS,TYPE,TYPE_ID,LINE,PACKAGE_TYPE,PRODUCT_NAME,PACKAGE_VOLUME,ASEBI_SPEED,BPM,BBN,BRAND,MODEL,THERMAL_TREATMENT,DESCRIPTION,COMMENTS,ANY_CONDITIONAL,STATUS,CONDITIONED_TO,CONDITIONAL_UP_TO,APPROVAL_DATE,OTHERS)
VALUES
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Pasteurized Milk','','13','','','','','','','','','','L 13 Envasadora Hema + Hydrolock
    1 ETAPA
    •	Leche Semidescremada con Chocolate Fortificada con Fibra, Vitaminas A y D Esterelizada Larga Vida GO TONI” 1 FASE
    Flex: 85°C @15, 14000L/h
    Hydrolock: 122°C @22 minutos','','','Approved','','','November 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Pasteurized Milk','','13','','','','','','','','','','L 13 Envasadora Hema + Hydrolock
    2 ETAPA
    •	Leche Esterelizada Chocolate 200 cc 2 fases
    1er fase: 139°C @ 4 seg Retención Flex FDA 
    2da Fase: 122°C @ 13 minutos Hydrolock','','','Approved','','','July 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Pasteurized Milk','','13','','','','','','','','','','L 13 Envasadora Hema + Hydrolock
    2 ETAPA
    •	Leche Semidescremada con Chocolate Fortificada con Fibra, Vitaminas A y D Esterelizada Larga Vida GO TONI
    1er fase: 139°C @ 4 seg Retención Flex FDA 
    2da Fase: 122°C @ 16 minutos Hydrolock
    •	Leche Semidescremada con Cereales Sabor a Coco Adicionada con Vitaminas y minerales Esterelizada GO TONI”
    1er fase: 139°C @ 4 seg Retención Flex FDA 
    2da Fase: 122°C @ 16 minutos Hydrolock
    •	Leche Descremada Sabor a Chocolate y Cereza fortificada con Calcio Zinc Vitaminas y adicionada con Hierro reducida en azúcar y grasa Esterelizada light 
    1er fase: 139°C @ 4 seg Retención Flex FDA ','','','Approved','','','August 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Pasteurized Milk','','13','','','','','','','','','','L 13 Envasadora Hema + Hydrolock
    2 ETAPA
    2da Fase: 122°C @ 16 minutos Hydrolock
    •	Leche Semidescremada sabor a Chocolate Esterelizada “Toni Chiqui 135 cc 
    •	Leche Semidescremada sabor a Frutilla Esterelizada “Toni Chiqui 135 cc 
    1er fase: 139°C @ 4 seg Retención Flex FDA 
    2da Fase: 120°C @ 14 minutos Hydrolock
    •	Cafelatto Doble Expresso 135 cc 
    1er fase: 139°C @ 4 seg Retención Flex FDA 
    2da Fase: 120°C @ 14 minutos Hydrolock','','','Approved','','','March 2020',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','15','','','','','','','','','','L 15 Osgood Yogurt
    •	Yogurt Entero sabor a frutilla y Hojuelas de maíz azucaradas “Toni Mix: 90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención.
    •	Yogurt Entero sabor a frutilla y Galletas con chispas sabor a chocolate “Toni Mix”: : 90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención','','','Approved','','','November 2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','15','','','','','','','','','','L 15 Osgood Yogurt
    •	Yogurt Entero Sabor a Vainilla y Galletas con Chispas Sabor a Chocolate 180g Mix Vainilla Galletas Chocolate
    90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención','','','Approved','','','February 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','16','','','','','','','','','','L16 Primo 16000
    •	Yogurt Entero sabor a frutilla y Hojuelas de maíz azucaradas “Toni Mix”: 90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención ','','','Approved','','','January 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','18','','','','','','','','','','L 18 Hema Yogurt
    •	Yogurt Entero bebible sabor a Frutilla “Toni”: 90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención','','','Approved','','','November 2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','18','','','','','','','','','','L 18 Hema Yogurt
    Yogurt Descremado Deslactosado Natural Light 950 g','','','Approved','','','April 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','18','','','','','','','','','','L 18 Hema Yogurt
    Yogurt Descremado Deslactosado Natural con Fruta Frutilla y Concentrado de Manzana 960 g','','','Approved','','','August 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','17','','','','','','','','','','L17 FOGG 54
    •	Yogurt Entero sabor a Frutilla, durazno y mora: : 90°C@ 15 s, 13000L/h. Adicional:  90C@ 485s de retención','','','Approved','','','February 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','17','','','','','','','','','','L17 FOGG 54
    •	Yogurt Entero sabor a Naranjilla 200g','','','Approved','','','March 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','17','','','','','','','','','','L17 FOGG 54
    •	Bebida de Yogurt. YORBETE, sabor a frutilla y durazno. 60% Yogurt, 40% Suero de leche','','','Approved','','','April 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','17','','','','','','','','','','L17 FOGG 54
    •	Yogurt Entero Natural sin Azúcar 190 g','','','Approved','','','August 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Yogurt','','17','','','','','','','','','','L17 FOGG 54
    •	Yogurt Natural Semidescremado Sabor Endulzado 190 g','','','Approved','','','September 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','11','','','','','','','','','','L11  BISIGNANO 1
    •	Manjar de Leche','','','Approved','','','April 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Cheese or Cream Cheese','','11','','','','','','','','','','L11  BISIGNANO 1
    •	Queso Crema','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Cheese or Cream Cheese','','12','','','','','','','','','','L12 Primo
    •	Queso crema','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','10','','','','','','','','','','L10 BISIGNANO 2
    •	Manjar de leche','','','Approved','','','April 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','14','','','','','','','','','','L14 OSGOOD
    •	Yogurt Cuchareable / Gelatina','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','14','','','','','','','','','','L14 OSGOOD
    •	Yogurt Semidescremado concentrado sabor a Frutilla Toni Griego” 150g
    •	Yogurt Semidescremado natural concentrado Toni Griego 150g','','','Approved','','','July 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','14','','','','','','','','','','L14 OSGOOD
    •	Yogurt Descremado Sabor a Vainilla Nutrimix Quinua 133g
    •	Yogurt Descremado Sabor a Frutilla   Nutrimix Muesli 137 g','','','Approved','','','October 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','14','','','','','','','','','','L14 OSGOOD
    •	Yogurt Semidescremado Concentrado con Base de Fruta Mango y Chía Toni Griego 150 g','','','Approved','','','July 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','14','','','','','','','','','','L14 OSGOOD
    •	Yogurt Semidescremado Concentrado con Base de Fruta Frutilla y Chía Toni Griego 150 g','','','Approved','','','September 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Cheese or Cream Cheese','','20','','','','','','','','','','L20 Form Fill & Seal
    Queso Crema Light Untable 50g','','','Approved','','','March 2020',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Jellies/
    Desserts','','20','','','','','','','','','','L20 Form Fill & Seal
    •	TO-NINO - Alimento Lacteo Infantil con Pulpa de Frutilla y Manzana  ','','','Approved','','','June 2020',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador Recepción Leche
    Marca: Tetrapak
    Modelo:TetraTherm Lacta 1 T5845165003
    Capacidad Nominal: 20000 lts/hr _ 
    Capacidad Real Operativa: 18000 lts/hr
    Temperatura: 75°C
    Tiempo: 15 seg','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador Helados 
    Marca: Tetrapak
    Modelo: TetraTherm Lacta Serie T5845160033
    Capacidad Nominal: 6000 lts/hr
    Temperatura: 85°C
    Tiempo: 15 seg de retención','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador de Gelatina
    Marca: TetraPak
    Modelo: TetraTherm Lacta -1 Serie: T5845165004 
    Capacidad Nominal: 10000 lts/hr
    Temperatura: 85°C
    Tiempo: 15 seg','','','Approved','','','2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador Área Mezcla de yogurt
    Marca: TetraPak
    Modelo: TetraTherm Lacta-1 Serie: T585140278
    Capacidad Nominal: 13000 lts/hr
    Temperatura: 90°C
    Tiempo: 15 seg + 485 seg ( Heat Holding)','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador UHT - Tetrapak
    Marca: TetraPak
    Modelo:TA Flex 100THE Serie: T5844150890
    Capacidad Nominal: 7000 lts/hr 
    Capacidad Real Operativa: 6200 lts/hr
    Temperatura por proceso. Tiempo Retención: 4 s. ','','','Approved','','','2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador UHT  -Leche de Sabores (conectado a Hydrolock y A3 Flex&A3 Compact Flex)
    Marca: TetraPak
    Modelo:TA Flex FDA Serie: T5844156083
    Capacidad Nominal: 
    Proceso Retorta Hydrolock:  14000 lts/hr , 85°C @ 4s 
    Proceso Aseptico:  14000 lts/hr , T de acuerdo a formula@ 4s. 
    Proceso UHT Aseptico Avenas+Maracuya+Leche: 7000L/h.
    •	Avena, Leche, Saborizadas Frutilla y Vainilla: 139°C@ 4s
    •	Leche Chocolatadas 142°C @4s','','','Approved','','','2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','LAAF','Heat Treatment & pasteurizer','','','','','','','','','','','','Pasteurizador de Quark ( Mezcla de Queso Crema)
    Marca: Tetrapak
    Modelo: TetraTherm Lacta Serie T5845160033
    Capacidad Nominal: 6000 lts/hr
    T = 85°C@ 15 s
    Termizador de Quark 
    Config. Para 4500 kg/hr x 1 min de retención','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área de Recepción de Leche 
    Descremadora Manual Tetra Alfast conectado a Pasteurizador TetraTherm Lacta 1 T5845165003','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área de Descremado 
    Crema enfriada a condiciones de 4°C
    Pasteurizador Technogel: 1200 L/hr@15 s
    Almacenamiento Enchaquetados< 4°C.
    Vida Util Crema pasteurizada: 3días de vida útil
    Vida Util Crema Congelada:6 meses max','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other processes','Other processes/equipment','','','','','','','','','','','','Sistema Ultrafiltración - Queso Crema Light','','','Approved','','','March 2020',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área Mezcla de yogurt','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área Maduración y Almacenamiento de yogurt','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área mezcla Leche de Sabores','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área Mezcla UHT - Tetrapack','','','Approved','','','April 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área mezcla Base de Helado y Queso crema','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área de Maduración y Desuerado Queso Crema','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área de proceso Manjar de leche','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Área de Mezcla y Almacenamiento de Gelatina','','','Approved','','','2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Cleaning and Sanitation ','CIP equipment','','','','','','','','','','','','Unidades de CIPs - Tetra Pak
    •	Leche Cruda
    •	Tanques asépticos
    •	Leche Pasteurizada
    •	Helados
    •	Gelatina
    •	Mezcla Leche Sabores/mezcla UHT
    •	Yogurt','','','Approved','','','2016',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Cleaning and Sanitation ','CIP equipment','','','','','','','','','','','','Tetra Pak - Queso Crema','','','Approved','','','May 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Cleaning and Sanitation ','CIP equipment','','','','','','','','','','','','CIP - Sepellec -UNIDAD CIP UL01-002. 
    Helados
    Modulo A2: Freezers:  13, 9, 12 / Tanques: 17, 18, 21, 22, 24.
    Modulo B2:  Freezers: 8, 10, 11 / Tanques: 15, 16, 19, 20, 23.
    Marmitas - Mezcla Manjar','','','Approved','','','October 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Tratamiento de Aguas Residuales','','','Approved','','','June 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Sistema de tratamiento terciario','','','Approved','','','July 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Other approvals','Other approvals','','','','','','','','','','','','Sistema de tratamiento cuaternario','','','Approved','','','October 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Other','','','','','','','','','','','','Helados CIP 3P-7P, aplica para L1 a L7','','','Approved','','','January 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Extension of commercial runs','','','','','','','','','','','','Extensión de Corridas de 24 a 36h horas en Leche de Sabores','','','Approved','','','October 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Extension of commercial runs','','','','','','','','','','','','Helados corridas extendidas 24 a 30Hr, aplica para L1 a L6','','','Approved','','','February 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Extension of commercial runs','','','','','','','','','','','','Aprobación de Extensión de Corridas de 24 a 36h en CIP Yogurt en Centro de Mezcla, Tanques de  Feeding y Envasadoras  ( L14 - L15- L16- L17- L18)','','','Approved','','','February 2019',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Other','','','','','','','','','','','','Aprobacion de Esquema de CIP 5 pasos en circuitos de limpieza de Leche Sabores, Queso Crema, Mezcla Base, Gelatina & CIP 142- Yogurt a Esquema 
    5P-3P-3P-5P-3P-3P-7P

    Permanecen en 5 pasos:
    CIP 140-141 (Leche Cruda & Pasteurizada):  Tanques de Almacenamiento.
    CIP Queso Crema: Almix, Tq Mezcla 1&2, Tq Maduracion 3, 4, Linea a Primo Queso, Linea Suero a Mz Yogurt, Linea a Osgood (L14).
    Alcip 300 Modulo 141 - UHT: Tanques Mezcla, Almix, Linea Esterilizador
    Alcip 300 Modulo 142 - Leche Sabores:  Almix Batch, Hema Leche Sabores
    Alcip 1:  Tq Aseptico UHT & Leche Sabores.
    Se excluyen de esta validación y se mantiene un esquema de 5P-7P:
    CIP 142:   Tq de Maduracion y Lineas de Frio por prevención de Fagos 
    CIP Sueco:  Linea de Fruta, Tanques Feeding por prevención de Fagos.','','','Approved','','','October 2017',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Other','','','','','','','','','','','','Aprobacion de Esquema de CIP 5 pasos en circuitos de limpieza de   Recepcion de leche a   Esquema  
    5P-3P-3P-5P-3P-3P-7P
    Aplica el esquema a: 
    MRU 1 - Filtros / MRU2- Filtros / Linea de leche pasteurizada 2 (Salida) / Linea de leche cruda, pasteurizador 2 (Entrada) / Entrada a placas de enfriamiento para crema Pasteurizador #2 / Salida de Placas de Enfriamiento de Crema Pasteurizador #2 / Linea de Leche Pasteurizada Hacia Mezcla de Leche de Sabores
    Permanecen en 5 pasos: 
    -Línea de pasteurizada HACIA mezcla de UHT (con receta de 5 pasos).
    -Línea de pasteurizada HACIA mezcla de Yogurt (con receta de 5 pasos).','','','Approved','','','September 2018',''),
    ('NULL','NULL','43831','Ecuador','Industrias Lácteas Toni Sa','4917','','','Cecilia Zamora','myriam.zamora@tonicorp.com','Juan Carlos López','juan.lopezpatino@tonicorp.com','Tatiana Araya','taraya@coca-cola.com','','Process Change','Rinser Removal','','','','','','','','','','','','Eliminación de Rinser de Agua Fogg 54.','','','Approved','','','February 2017',''),
    ('NULL','NULL','43831','Costa Rica','Calle Blancos','6412','','','Rodolfo Garita Serrano','rodolfo.garita@kof.com.mx','Elvis Alvarez Valdez','elvis.alvarez@kof.com.mx','Luciana Piza','lpiza@coca-cola.com','','Water Treatment','Conventional Treatment ','','','','','','','','','','','','','','','Approved','','','January 2010',''),
    ('NULL','NULL','43831','Costa Rica','Calle Blancos','6412','','','Rodolfo Garita Serrano','rodolfo.garita@kof.com.mx','Elvis Alvarez Valdez','elvis.alvarez@kof.com.mx','Luciana Piza','lpiza@coca-cola.com','','Water Treatment','Conventional - In line flocculation','','','','','','','','','','','','Tratamiento convencional - floculación en línea','','','Approved','','','January 2010',''),
    ('NULL','NULL','43831','Costa Rica','Calle Blancos','6412','','','Rodolfo Garita Serrano','rodolfo.garita@kof.com.mx','Elvis Alvarez Valdez','elvis.alvarez@kof.com.mx','Luciana Piza','lpiza@coca-cola.com','','Water Treatment','Reverse Osmosis ','','','','','','','','','','','','','','','Approved','','','January 2010',''),
    ('NULL','NULL','43831','Costa Rica','Calle Blancos','6412','','','Rodolfo Garita Serrano','rodolfo.garita@kof.com.mx','Elvis Alvarez Valdez','elvis.alvarez@kof.com.mx','Luciana Piza','lpiza@coca-cola.com','','Water Treatment','Conventional Treatment ','','','','','','','','','','','','Tratamiento de agua suavizada
    Enfriamiento
    Neutralización
    Filtración precapa
    Filtración arena
    Suavizado
    Desinfección con cloro
    Pulidor','','','Approved','','','January 2010','')";
executeQuery($query);
exit();
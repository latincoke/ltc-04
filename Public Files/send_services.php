<?php
    $i_folio = $_POST['folio'];
    $s_email = $_POST['email'];
    $s_stage = $_POST['stage'];

    $json_result = array();
    
    // Busca la solicitud
    $REQUEST=executeQuery(
        "SELECT
            ID,
            USER_UID,
            CASE_NUMBER,
            APP_UID,
            VALIDATION_STAGE,
            STATUS,
            D_REQUEST_DATE,
            S_COUNTRY,
            S_PLANT_NAME,
            S_PLANT_ID,
            S_REQUESTER_NAME,
            S_REQUESTER_EMAIL,
            S_MANAGER_NAME,
            S_MANAGER_EMAIL,
            S_PLANT_MANAGER_NAME,
            S_PLANT_MANAGER_EMAIL,
            S_TOM_DESIGNATED,
            S_TOM_EMAIL
        FROM
            PMT_PRP_REQUEST
	    WHERE
            CASE_NUMBER = '" . $i_folio . "' AND
            S_REQUESTER_EMAIL = '" . $s_email . "'
        ORDER BY
            ID DESC
        LIMIT 1
    ");
    if (isset($REQUEST[1]['USER_UID'])) {
        if ($REQUEST[1]['VALIDATION_STAGE'] == $s_stage && $REQUEST[1]['VALIDATION_STAGE'] != $REQUEST[1]['STATUS'] && $REQUEST[1]['STATUS'] != 'Conditioned' && $REQUEST[1]['STATUS'] != 'Approved') {
            $json_result['ID']                    = isset($REQUEST[1]['ID']) ? $REQUEST[1]['ID'] : "ERROR";
            $json_result['USER_UID']              = isset($REQUEST[1]['USER_UID']) ? $REQUEST[1]['USER_UID'] : "ERROR";
            $json_result['CASE_NUMBER']           = isset($REQUEST[1]['CASE_NUMBER']) ? $REQUEST[1]['CASE_NUMBER'] : "ERROR";
            $json_result['APP_UID']               = isset($REQUEST[1]['APP_UID']) ? $REQUEST[1]['APP_UID'] : "ERROR";
            $json_result['VALIDATION_STAGE']      = isset($REQUEST[1]['VALIDATION_STAGE']) ? $REQUEST[1]['VALIDATION_STAGE'] : "ERROR";
            $json_result['STATUS']                = isset($REQUEST[1]['STATUS']) ? $REQUEST[1]['STATUS'] : "ERROR";
            $json_result['D_REQUEST_DATE']        = isset($REQUEST[1]['D_REQUEST_DATE']) ? $REQUEST[1]['D_REQUEST_DATE'] : "ERROR";
            $json_result['S_COUNTRY']             = isset($REQUEST[1]['S_COUNTRY']) ? $REQUEST[1]['S_COUNTRY'] : "ERROR";
            $json_result['S_PLANT_NAME']          = isset($REQUEST[1]['S_PLANT_NAME']) ? $REQUEST[1]['S_PLANT_NAME'] : "ERROR";
            $json_result['S_PLANT_ID']            = isset($REQUEST[1]['S_PLANT_ID']) ? $REQUEST[1]['S_PLANT_ID'] : "ERROR";
            $json_result['S_REQUESTER_NAME']      = isset($REQUEST[1]['S_REQUESTER_NAME']) ? $REQUEST[1]['S_REQUESTER_NAME'] : "ERROR";
            $json_result['S_REQUESTER_EMAIL']     = isset($REQUEST[1]['S_REQUESTER_EMAIL']) ? $REQUEST[1]['S_REQUESTER_EMAIL'] : "ERROR";
            $json_result['S_MANAGER_NAME']        = isset($REQUEST[1]['S_MANAGER_NAME']) ? $REQUEST[1]['S_MANAGER_NAME'] : "ERROR";
            $json_result['S_MANAGER_EMAIL']       = isset($REQUEST[1]['S_MANAGER_EMAIL']) ? $REQUEST[1]['S_MANAGER_EMAIL'] : "ERROR";
            $json_result['S_PLANT_MANAGER_NAME']  = isset($REQUEST[1]['S_PLANT_MANAGER_NAME']) ? $REQUEST[1]['S_PLANT_MANAGER_NAME'] : "ERROR";
            $json_result['S_PLANT_MANAGER_EMAIL'] = isset($REQUEST[1]['S_PLANT_MANAGER_EMAIL']) ? $REQUEST[1]['S_PLANT_MANAGER_EMAIL'] : "ERROR";
            $json_result['S_TOM_DESIGNATED']      = isset($REQUEST[1]['S_TOM_DESIGNATED']) ? $REQUEST[1]['S_TOM_DESIGNATED'] : "ERROR";
            $json_result['S_TOM_EMAIL']           = isset($REQUEST[1]['S_TOM_EMAIL']) ? $REQUEST[1]['S_TOM_EMAIL'] : "ERROR";
            
            $REQUEST_DETAIL=executeQuery(
                "SELECT
                    S_CPK,
                    S_MICROBIOLOGY,
                    S_NEXT_LAST,
                    S_ALERT_RELATED_PROCESS,
                    S_CONSUMER_COMPLAINTS,
                    S_GAO,
                    S_GAO_DETAIL,
                    S_IMCR_YEAR,
                    S_IMCR_DETAIL,
                    S_PROJECT_TYPE,
                    S_TYPE,
                    S_VALIDATION_TYPE,
                    S_PROCESS_CHANGE_DETAIL,
                    S_LINE,
                    S_PACKAGE_TYPE,
                    S_BBN,
                    S_PRODUCT_NAME,
                    S_BRAND,
                    S_MODEL,
                    S_THERMAL_TREATMENT,
                    S_DESCRIPTION,
                    S_DESIGN_COMMENTS,
                    S_DESIGN_COMMENTS_KO,
                    S_INSTALL_COMMENTS,
                    S_INSTALL_COMMENTS_KO,
                    S_OPERATION_COMMENTS,
                    S_OPERATION_COMMENTS_KO,
                    S_PERFORMANCE_COMMENTS,
                    S_PERFORMANCE_COMMENTS_KO
                FROM
                    PMT_PRP_REQUEST_DETAIL
                WHERE
                    CASE_NUMBER = " . $REQUEST[1]['CASE_NUMBER'] . "
                ORDER BY
                    ID DESC
                LIMIT 1
            "
            );
            $json_result['S_CPK']                   = isset($REQUEST_DETAIL[1]['S_CPK']) ? $REQUEST_DETAIL[1]['S_CPK'] : "ERROR";
            $json_result['S_MICROBIOLOGY']          = isset($REQUEST_DETAIL[1]['S_MICROBIOLOGY']) ? $REQUEST_DETAIL[1]['S_MICROBIOLOGY'] : "ERROR";
            $json_result['S_NEXT_LAST']             = isset($REQUEST_DETAIL[1]['S_NEXT_LAST']) ? $REQUEST_DETAIL[1]['S_NEXT_LAST'] : "ERROR";
            $json_result['S_ALERT_RELATED_PROCESS'] = isset($REQUEST_DETAIL[1]['S_ALERT_RELATED_PROCESS']) ? $REQUEST_DETAIL[1]['S_ALERT_RELATED_PROCESS'] : "ERROR";
            $json_result['S_CONSUMER_COMPLAINTS']   = isset($REQUEST_DETAIL[1]['S_CONSUMER_COMPLAINTS']) ? $REQUEST_DETAIL[1]['S_CONSUMER_COMPLAINTS'] : "ERROR";
            $json_result['S_GAO']                   = isset($REQUEST_DETAIL[1]['S_GAO']) ? $REQUEST_DETAIL[1]['S_GAO'] : "ERROR";
            $json_result['S_GAO_DETAIL']            = isset($REQUEST_DETAIL[1]['S_GAO_DETAIL']) ? $REQUEST_DETAIL[1]['S_GAO_DETAIL'] : "ERROR";
            $json_result['S_IMCR_YEAR']             = isset($REQUEST_DETAIL[1]['S_IMCR_YEAR']) ? $REQUEST_DETAIL[1]['S_IMCR_YEAR'] : "ERROR";
            $json_result['S_IMCR_DETAIL']           = isset($REQUEST_DETAIL[1]['S_IMCR_DETAIL']) ? $REQUEST_DETAIL[1]['S_IMCR_DETAIL'] : "ERROR";
            $json_result['S_PROJECT_TYPE']          = isset($REQUEST_DETAIL[1]['S_PROJECT_TYPE']) ? $REQUEST_DETAIL[1]['S_PROJECT_TYPE'] : "ERROR";
            $json_result['S_TYPE']                  = isset($REQUEST_DETAIL[1]['S_TYPE']) ? $REQUEST_DETAIL[1]['S_TYPE'] : "ERROR";
            $json_result['S_VALIDATION_TYPE']       = isset($REQUEST_DETAIL[1]['S_VALIDATION_TYPE']) ? $REQUEST_DETAIL[1]['S_VALIDATION_TYPE'] : "ERROR";
            $json_result['S_PROCESS_CHANGE_DETAIL'] = isset($REQUEST_DETAIL[1]['S_PROCESS_CHANGE_DETAIL']) ? $REQUEST_DETAIL[1]['S_PROCESS_CHANGE_DETAIL'] : "ERROR";
            $json_result['S_LINE']                  = isset($REQUEST_DETAIL[1]['S_LINE']) ? $REQUEST_DETAIL[1]['S_LINE'] : "ERROR";
            $json_result['S_PACKAGE_TYPE']          = isset($REQUEST_DETAIL[1]['S_PACKAGE_TYPE']) ? $REQUEST_DETAIL[1]['S_PACKAGE_TYPE'] : "ERROR";
            $json_result['S_BBN']                   = isset($REQUEST_DETAIL[1]['S_BBN']) ? $REQUEST_DETAIL[1]['S_BBN'] : "ERROR";
            $json_result['S_PRODUCT_NAME']                   = isset($REQUEST_DETAIL[1]['S_PRODUCT_NAME']) ? $REQUEST_DETAIL[1]['S_PRODUCT_NAME'] : "ERROR";
            $json_result['S_BRAND']                 = isset($REQUEST_DETAIL[1]['S_BRAND']) ? $REQUEST_DETAIL[1]['S_BRAND'] : "ERROR";
            $json_result['S_MODEL']                 = isset($REQUEST_DETAIL[1]['S_MODEL']) ? $REQUEST_DETAIL[1]['S_MODEL'] : "ERROR";
            $json_result['S_THERMAL_TREATMENT']     = isset($REQUEST_DETAIL[1]['S_THERMAL_TREATMENT']) ? $REQUEST_DETAIL[1]['S_THERMAL_TREATMENT'] : "ERROR";
            $json_result['S_DESCRIPTION']           = isset($REQUEST_DETAIL[1]['S_DESCRIPTION']) ? $REQUEST_DETAIL[1]['S_DESCRIPTION'] : "ERROR";
            $json_result['S_DESIGN_COMMENTS']         = isset($REQUEST_DETAIL[1]['S_DESIGN_COMMENTS']) ? $REQUEST_DETAIL[1]['S_DESIGN_COMMENTS'] : "ERROR";
            $json_result['S_DESIGN_COMMENTS_KO']      = isset($REQUEST_DETAIL[1]['S_DESIGN_COMMENTS_KO']) ? $REQUEST_DETAIL[1]['S_DESIGN_COMMENTS_KO'] : "ERROR";
            $json_result['S_INSTALL_COMMENTS']        = isset($REQUEST_DETAIL[1]['S_INSTALL_COMMENTS']) ? $REQUEST_DETAIL[1]['S_INSTALL_COMMENTS'] : "ERROR";
            $json_result['S_INSTALL_COMMENTS_KO']     = isset($REQUEST_DETAIL[1]['S_INSTALL_COMMENTS_KO']) ? $REQUEST_DETAIL[1]['S_INSTALL_COMMENTS_KO'] : "ERROR";
            $json_result['S_OPERATION_COMMENTS']      = isset($REQUEST_DETAIL[1]['S_OPERATION_COMMENTS']) ? $REQUEST_DETAIL[1]['S_OPERATION_COMMENTS'] : "ERROR";
            $json_result['S_OPERATION_COMMENTS_KO']   = isset($REQUEST_DETAIL[1]['S_OPERATION_COMMENTS_KO']) ? $REQUEST_DETAIL[1]['S_OPERATION_COMMENTS_KO'] : "ERROR";
            $json_result['S_PERFORMANCE_COMMENTS']    = isset($REQUEST_DETAIL[1]['S_PERFORMANCE_COMMENTS']) ? $REQUEST_DETAIL[1]['S_PERFORMANCE_COMMENTS'] : "ERROR";
            $json_result['S_PERFORMANCE_COMMENTS_KO'] = isset($REQUEST_DETAIL[1]['S_PERFORMANCE_COMMENTS_KO']) ? $REQUEST_DETAIL[1]['S_PERFORMANCE_COMMENTS_KO'] : "ERROR";

            // Documentos
            $DOCUMENTS=executeQuery(
                "(SELECT
                    ID,
                    CASE_NUMBER,
                    APP_UID,
                    APP_DOCUMENT_UID,
                    APP_DOCUMENT_FILENAME,
                    EXTENTION,
                    APP_DOCUMENT_FIELDNAME,
                    LINK
                FROM
                    PMT_PRP_REQUEST_DOCUMENTS_DESIGN
                WHERE
                    CASE_NUMBER = '" . $i_folio . "'
                ORDER BY
                    ID DESC
                ) UNION (
                    SELECT
                        ID,
                        CASE_NUMBER,
                        APP_UID,
                        APP_DOCUMENT_UID,
                        APP_DOCUMENT_FILENAME,
                        EXTENTION,
                        APP_DOCUMENT_FIELDNAME,
                        LINK
                    FROM
                        PMT_PRP_REQUEST_DOCUMENTS_INSTALLING
                    WHERE
                        CASE_NUMBER = '" . $i_folio . "'
                    ORDER BY
                        ID DESC
                ) UNION (
                    SELECT
                        ID,
                        CASE_NUMBER,
                        APP_UID,
                        APP_DOCUMENT_UID,
                        APP_DOCUMENT_FILENAME,
                        EXTENTION,
                        APP_DOCUMENT_FIELDNAME,
                        LINK
                    FROM
                        PMT_PRP_REQUEST_DOCUMENTS_OPERATION
                    WHERE
                        CASE_NUMBER = '" . $i_folio . "'
                    ORDER BY
                        ID DESC
                ) UNION (
                    SELECT
                        ID,
                        CASE_NUMBER,
                        APP_UID,
                        APP_DOCUMENT_UID,
                        APP_DOCUMENT_FILENAME,
                        EXTENTION,
                        APP_DOCUMENT_FIELDNAME,
                        LINK
                    FROM
                        PMT_PRP_REQUEST_DOCUMENTS_PERFORMANCE
                    WHERE
                        CASE_NUMBER = '" . $i_folio . "'
                    ORDER BY
                        ID DESC
                )
            "
            );
            $json_result['DOCUMENTS'] = $DOCUMENTS;

            // Comentarios
            $COMMENTS=executeQuery(
                "SELECT
                    ID,
                    CASE_NUMBER,
                    COMMENT,
                    USER_UID,
                    USER_NAME,
                    COMMENT_DATE 
                FROM
                    PMT_MP_COMMENTS
                WHERE
                    CASE_NUMBER = '" . $i_folio . "'"
            );
            $S_COMMENT_GRID = array();
            for ($i = 1; $i <=  count($COMMENTS); $i++) {
                array_push(
                    $S_COMMENT_GRID,
                    array(
                        'USER_NAME'     => $COMMENTS[$i]['USER_NAME'],
                        'COMMENT'       => $COMMENTS[$i]['COMMENT'],
                        'COMMENT_DATE'  => $COMMENTS[$i]['COMMENT_DATE'])
                );
            }
            $json_result['S_COMMENT_GRID']  = $S_COMMENT_GRID;

            // ASEBI
            $ASEBI=executeQuery(
                "SELECT
                    PRODUCT_NAME,
                    SPEED,
                    VOLUME,
                    PACKAGE_TYPE
                FROM
                    PMT_PRP_REQUEST_ASEBI_DETAILS
                WHERE
                    CASE_NUMBER = '" . $i_folio . "'"
            );
            $S_ASEBI_GRID = array();
            for ($i = 1; $i <=  count($ASEBI); $i++) {
                array_push(
                    $S_ASEBI_GRID,
                    array(
                        'PRODUCT_NAME'     => $ASEBI[$i]['PRODUCT_NAME'],
                        'SPEED'       => $ASEBI[$i]['SPEED'],
                        'VOLUME'       => $ASEBI[$i]['VOLUME'],
                        'PACKAGE_TYPE'  => $ASEBI[$i]['PACKAGE_TYPE'])
                );
            }
            $json_result['S_ASEBI_GRID']  = $S_ASEBI_GRID;
        } else {
            $json_result['USER_UID'] =  "STAGE_ERROR";
        }
    } else {
        $json_result['USER_UID'] =  "ERROR";
    }
    print json_encode($json_result);